<?php


namespace App\Models;


use Core\Application;
use Core\Db\Db;
use Core\Db\Model;

class Users extends Model
{

    static protected function tableName()
    {
        return "users";
    }

}