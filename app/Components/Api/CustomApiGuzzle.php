<?php
namespace App\Components\Api;

use Core\Contracts\ComponentAbstract;
use GuzzleHttp\ClientInterface;

/**
 * @method getUser()
 */
class CustomApiGuzzle extends ComponentAbstract
{
    /**
     * @var ClientInterface
     */
    protected $client;
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function getUsers(){

        $response = $this->client->request('GET', 'public-api/users?access-token=BgTFcyr8_OidjC7EHgsIDwWigIyHd1Gy_LVb ');
        $body =  $response->getBody();
        $array = \GuzzleHttp\json_decode($body, true);
       // var_dump($array['result']);
        $iterator = new \ArrayIterator($array['result']);

            foreach ($iterator as $user)
            {
                    echo '<b>' . 'id: ' . $user['id'] . '</b>' .
                        '<br>';
                    echo 'first_name: ' . $user['first_name'] .
                        '<br>';
                    echo 'last_name: ' . $user['last_name'] .
                        '<br>';
                    echo 'gender: ' . $user['gender'] .
                        '<br>';
                    echo 'dob: ' . $user['dob'] .
                        '<br>';
                    echo 'email: ' . $user['email'] .
                        '<br>';
                    echo 'phone: ' . $user['phone'] .
                        '<br>';
                    echo 'website: ' . $user['website'] .
                        '<br>';
                    echo 'address: ' . $user['address'] .
                        '<br>';
                    echo 'status: ' . $user['status'] .
                        '<br>';
                    echo 'website: ' . $user['website'] .
                        '<br>' . '<br>' . '<br>';;
        }

    }
    public function bootstrap()
    {
        // TODO: Implement bootstrap() method.
    }
}