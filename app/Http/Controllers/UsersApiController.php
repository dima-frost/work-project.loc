<?php


namespace App\Http\Controllers;
use App\Components\Api\CustomApiGuzzle;
use Core;
use Core\Application;


class UsersApiController
{
    public function getUsers()
    {
        /** @var CustomApiGuzzle $app */
        $app =  Application::getInstance()->get(CustomApiGuzzle::class);
      $app->getUsers();
    }

}