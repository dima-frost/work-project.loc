<?php


namespace App\Http\Controllers;


use Core\Db\PdoQueryBuilder;
use Core\Db\PdoSqlConnection;
use Core\Mailer\Mailer;

class IndexController
{
    public function indexAction()
    {
        echo 'index';
        $mailer = new Mailer();
        $mailer->send('test');
    }
}