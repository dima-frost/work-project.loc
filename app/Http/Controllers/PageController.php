<?php


namespace App\Http\Controllers;


class PageController
{
    public function viewAction($id)
    {
        echo 'Page view with id ' . $id;
    }
}