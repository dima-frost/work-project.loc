<?php


namespace App\Http\Controllers;




use App\Models\Users;
use Core\Db\Model;
use Core\Db\QueryBuilder;
use Core\Mailer\Mailer;
use Core\Observer\ObserverManager;
use Core\Observer\SubjectInterface;

class UsersController
{

    /**
     * @var SubjectInterface
     */
    protected $observerManager;
    public function __construct()
    {
        $this->observerManager = new ObserverManager();
        $listener = new Mailer();
        $this->observerManager->attach($listener, '*');
    }

    public function create($email, $first_name, $last_name, $password){
        $pass = md5($password);
        $newUser = Users::createModel(
            [
                'email' => $email,
                'first_name'=>$first_name,
                'last_name'=>$last_name,
                'password' => $pass,
            ]

        )->create();
        if ($newUser->save()) {
            echo 'Пользователь создан';
            $this->observerManager->notify('создан', [
                'email' => $email,
                'first_name' =>$first_name,
                'last_name' => $last_name,
            ]);
        }

    }
    public function read($email){
       $user = Users::find(['email'=>$email])->asArray()->one();
       print_r($user);
    }

    public function update($email, $first_name = null, $last_name= null, $password= null){
        /** @var Model $user */
        $user = Users::find()->where(['email' => $email])->one();
        if($first_name != null) $user->first_name = $first_name ;
        if ($last_name != null) $user->last_name = $last_name;
        if ($password != null ){
            $pass= md5($password);
            $user->password = $pass;}
        if ($user->save()) {
            echo 'Пользователь обновлен';
            $this->observerManager->notify('обновлен', [
                'email' => $user->email,
                'first_name' =>$user->first_name,
                'last_name' => $user->last_name,
            ]);
        }
    }
    public function delete($email)
    {
        /** @var Model $user */
        $user = Users::find()->where(['email' => $email])->one();
        $user->delete();
        if ($user->save()) {
            echo 'Пользователь удален';
            $this->observerManager->notify('удален', [
                'email' => $user->email,
                'first_name' =>$user->first_name,
                'last_name' => $user->last_name,
            ]);
        }

    }
    
}