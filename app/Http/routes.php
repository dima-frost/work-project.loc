<?php

/**
 * @var \Core\Routing\Router $this
 */

$this->get('/user/delete', [new \App\Http\Controllers\UsersController(), 'delete']);

$this->get('/user/update', [new \App\Http\Controllers\UsersController(), 'update']);

$this->get('/user/read', [new \App\Http\Controllers\UsersController(), 'read']);

$this->get('/user/create', [new \App\Http\Controllers\UsersController(), 'create']);


$this->get('/users-api', [new \App\Http\Controllers\UsersApiController(), 'getUsers']);

$this->get('/products/show', [new \App\Http\Controllers\ProductsController(), 'show']);

$this->get('/', [new \App\Http\Controllers\IndexController(), 'indexAction']);
