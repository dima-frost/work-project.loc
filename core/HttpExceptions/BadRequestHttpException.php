<?php


namespace Core\HttpExceptions;


use Throwable;

class BadRequestHttpException extends HttpException
{
    protected $message = "Bad Request";
    protected $code = 400;

}