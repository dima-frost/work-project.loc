<?php


namespace Core\HttpExceptions;


class MethodNotAllowedHttpExceptions extends HttpException
{
    protected  $message = "MethodNotAllowed";
    protected  $code = 405;
}