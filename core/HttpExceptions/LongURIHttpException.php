<?php


namespace Core\HttpExceptions;


class LongURIHttpException extends HttpException
{
    protected  $message = "Request-URI Too Long";
    protected  $code = 414;

}