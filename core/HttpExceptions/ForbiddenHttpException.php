<?php


namespace Core\HttpExceptions;


class ForbiddenHttpException extends HttpException
{
    protected $message = "Forbidden";
    protected  $code = 403;
}