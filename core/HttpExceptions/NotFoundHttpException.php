<?php


namespace Core\HttpExceptions;


class NotFoundHttpException extends HttpException
{
    protected  $message = "Not Found";
    protected  $code = 404;
}