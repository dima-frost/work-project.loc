<?php


namespace Core\Logger;
/**
 * Class FileWriterLogs  Наследует интерфейс WriterInterface так же как класс DbWriterLogs
 *
 * Данный класс используеться для записи логов в файл, если файл ранее не был создан, он создаеться автоматически.
 *
 * @package Core\Logger
 */

class FileWriterLogs  implements WriterInterface
{
    private $filePath;

    public function setFilePath()
    {
        return $this->filePath = $_SERVER['DOCUMENT_ROOT'] . '/../storage/logs/default.log';
    }
    public function __construct()
    {
        $this->setFilePath();
        if(!file_exists($this->filePath))
        {
            touch($this->filePath);
        }
    }

    /**
     * @param array $data Массив который с помощью функции implode() преобразуеться в строку для дальнеейшей записи в файл
     *
     */
    public function writer(array $data)
    {   $dataLog = implode(' ', $data);
        file_put_contents($this->filePath, $dataLog . PHP_EOL , FILE_APPEND);
    }
}