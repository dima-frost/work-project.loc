<?php


namespace Core\Logger;

use Psr\Log\AbstractLogger;

/**
 * Class Logger
 * @package Core\Logger
 */
class Logger extends AbstractLogger
{
    /**
     * @var FormatterInterface
     */
    private $formatter;
    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * Logger constructor.
     * @param FormatterInterface $formatter
     * @param WriterInterface $writer
     */
    public function __construct(FormatterInterface $formatter, WriterInterface $writer)
    {
        $this->formatter = $formatter;
        $this->writer = $writer;
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        $this->writer->writer($this->formatter->formatter($level, $message, $context));
    }

    public function bootstrap()
    {
        // TODO: Implement bootstrap() method.
    }
}