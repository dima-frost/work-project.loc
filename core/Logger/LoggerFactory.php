<?php


namespace Core\Logger;
/**
 * Class LoggerFactory Данный клас использует фабричный метод проектирования
 * @package Core\Logger
 */
class LoggerFactory
{
    /**
     * @return Logger
     */
    public function createInstance()
    {
        return $this->createConcreteInstance();
    }

    public function createConcreteInstance()
    {
        $writer = new DbWriterLogs();
        $formatter = new Formatter();
        return new Logger( $formatter, $writer);
    }
}