<?php


namespace Core\Logger;


use Core\Application;
use Core\Db\Db;
use Core\Db\MysqlConnection;

/**
 * Class DbWriterLogs Наследует интерфейс WriterInterface так же как класс FileWriterLogs
 *
 * Класс используеться для записи логов в базу данных, в данном  класе инстациирован класс MysqlConnection который создан по
 * патерну проектирования Singleton и который отвечает за соедениие с базов данных.
 *
 * @package Core\Logger
 */

class DbWriterLogs implements WriterInterface
{
    /**
     * @param  array $data Массив который содержит дату записи,  уровень ошыбки, сообщение и масив контекста.
     */
    public function writer(array $data)
    {
        $link= Application::getInstance()->get(Db::class);


        $query = "INSERT INTO `mvc_logs` (`date_log`, `level_log`, `message`, `context`)
        VALUES ("
            . "'" . $data["DATE"] . "', '" . $data["LEVEL"]. "', '" . $data["MESSAGE"] . "', '" . $data["CONTEXT"] . "')";


        $link->query($query);
    }
}