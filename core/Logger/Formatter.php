<?php


namespace Core\Logger;
/**
 * Class Formatter
 * @package Core\Logger Используеться для форматирование логов.
 */
class Formatter  implements FormatterInterface
{
    /**
     * @param $level
     * @param $message
     * @param array $context
     * @return array
     */
    public function formatter($level, $message, array $context)
    {
        return [
            "DATE" =>  date('Y-m-d H:i:s'),
            "LEVEL" => $level,
            "MESSAGE" => $message,
            "CONTEXT" => trim(implode("; ", $context))
        ];
    }
}