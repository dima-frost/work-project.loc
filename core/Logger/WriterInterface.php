<?php


namespace Core\Logger;
/**
 * Interface WriterInterface Общий интерфейс для классов которые используються для написания логов.
 * @package Core\Logger
 */

interface WriterInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function writer(array $data);

}