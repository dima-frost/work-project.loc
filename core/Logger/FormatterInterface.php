<?php


namespace Core\Logger;
/**
 * Interface FormatterInterface Общий интерфейс для классов которые используються для форматирования логов.
 * @package Core\Logger
 */

interface FormatterInterface
{
     public function formatter($level, $message, array $context);

}