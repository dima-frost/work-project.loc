<?php


namespace Core\ErrorHandler;


use Core\Logger\Logger;
use Core\Logger\LoggerFactory;
use Error;

/**
 * Class ErrorHandler Класс для обработки исключений
 * @package Core\ErrorHandler
 */
class ErrorHandler
{
    /**
     * Метод обработчик исключений, улавлювает ошибки и пишет логи.
     *
     * @param $errNo int Код ошибки;
     * @param $errMessage string Сообщение;
     * @param $errFile string Путь к файлу;
     * @param $errLine int Строка в файле;
     */
    function errorHandler($errNo, $errMessage, $errFile, $errLine)
    {
        $code = "error code: " .$errNo;
        $line = "in line: " . $errLine;
        $file = "in file: " . $errFile;
        $logger = (new LoggerFactory())->createInstance();
        $logger->error($errMessage, [$code, $line, $file]);
    }
}