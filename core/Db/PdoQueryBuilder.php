<?php


namespace Core\Db;


class PdoQueryBuilder
{   private $table;
    private $select = '*';
    private $where;
    private $order;
    private $limit = 20;
    private $offset;


    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function select($select)
    {
        $this->select = implode(', ', $select);
        return $this;
    }

    public function where($where)
    {
        $whereArr = [];
        foreach ($where as $key => $value){
            $whereArr[] = $key . "=" . "'". $value ."'";
        }
        $this->where = implode(", ", $whereArr);
        return $this;
    }

    public function order($order)
    {
        $orderArr = [];
        foreach ($order as $key => $value){
            $orderArr[] = $key . " " . $value;
        }
        $this->order = implode(", ", $orderArr);
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;

    }

    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function build()
    {   $select = "SELECT  $this->select";
        $from = "FROM  $this->table";
        if(empty($this->where))
        {
            $where = "";
        }else {
            $where = "WHERE   $this->where";
        }
        if(empty($this->order))
        {
            $order = "";
        }else {
            $order = "ORDER BY  $this->order";
        }
        if(empty($this->offset))
        {
            $offset = "";
        }else {
            $offset = "OFFSET  $this->offset";
        }
        $limit = "LIMIT  $this->limit";

        $query = $select . " " . $from .  " " . $where .  " " . $order .  " " . $limit . " " . $offset;
        return $query;
    }
    public function one()
    {  $connect = PdoSqlConnection::getInstance()->dbConnect();
        $stm = $connect->query($this->build());
        if ($stm) {
            return $stm->fetch();
        }
        return null;
    }
    public function all()
    {   $connect = PdoSqlConnection::getInstance()->dbConnect();
       $stm = $connect->query($this->build());
        if ($stm) {
            return $stm->fetchAll();
        }
        return null;
    }
}