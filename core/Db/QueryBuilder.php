<?php


namespace Core\Db;


use Core\Application;

class QueryBuilder implements QueryBuilderInterface
{   protected $db;
    private $table;
    private $select = '*';
    private $where;
    private $order;
    private $limit = 20;
    private $offset;
    private $link;
    private $insertInto;
    private $set;
    private $values;

    protected $asArray;

    protected $modelClass;
    public function __construct()
    {$this->db = Application::getInstance()->get(Db::class);
    }

    public function asArray()
    {
        $this->asArray = true;

        return $this;
    }

    public function setModel($modelClass)
    {
        $this->modelClass = $modelClass;

        return $this;
    }


    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function select($select)
    {
        $this->select = implode(', ', $select);
        return $this;
    }

    public function where($where)
    {
        $whereArr = [];
        foreach ($where as $key => $value){
            $whereArr[] = $key . "=" . "'". $value ."'";
        }
       $this->where = implode("AND ", $whereArr);
        return $this;
    }

    public function order($order)
    {
        $orderArr = [];
        foreach ($order as $key => $value){
            $orderArr[] = $key . " " . $value;
        }
        $this->order = implode(", ", $orderArr);
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;

    }

    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function build()
    {
        $select = "SELECT  $this->select";
        $from = "FROM  $this->table";
        if(empty($this->where))
        {
            $where = "";
        }else {
            $where = "WHERE   $this->where";
        }
        if(empty($this->order))
        {
            $order = "";
        }else {
            $order = "ORDER BY  $this->order";
        }
        if(empty($this->offset))
        {
            $offset = "";
        }else {
            $offset = "OFFSET  $this->offset";
        }
        $limit = "LIMIT  $this->limit";

        $query = $select . " " . $from .  " " . $where .  " " . $order .  " " . $limit . " " . $offset;
        return   $query;
    }

    public function one()
    {
        $this->limit = 1;
        $sql = $this->build();
        $response = $this->db->query($sql);

        $row = mysqli_fetch_assoc($response);
        if (
            !$this->asArray
            && $this->modelClass
            && is_a($this->modelClass, ActiveRecordInterface::class, true)
        ) {
            return $this->modelClass::createModel($row);
        }

        return $row;

    }
    public function all()
    {
        $this->limit = 1;
        $sql = $this->build();
        $response = $this->db->query($sql);

        $row = mysqli_fetch_assoc($response);
        if (
            !$this->asArray
            && $this->modelClass
            && is_a($this->modelClass, ActiveRecordInterface::class, true)
        ) {
            return $this->modelClass::createModels($rows);
        }

        return $rows;
    }
    public function reset()
    {
        $this->table = null;
        $this->select = ['*'];
        $this->where = [];
        $this->limit = '';
        $this->offset = '';
        $this->order = [];
    }
}