<?php


namespace Core\Db;


interface QueryBuilderInterface
{
    public  function table($table);
    public  function select($select);
    public  function where($where);
    public function order($order);
    public function limit($limit);
    public function offset($offset);
    public function build();
    public function one();
    public function all();


}