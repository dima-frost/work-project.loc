<?php


namespace Core\Db;


use Core\Contracts\ComponentAbstract;
use Core\Contracts\ComponentFactoryAbstract;
use Core\Contracts\ContainerAbstract;

class DbFactory extends ComponentFactoryAbstract
{

    public function createConcreteInstance(): ComponentAbstract
    {
        $db = Db::getInstance();
        if (!$db->isConfigured()) {
            $config = $this->app->config(ContainerAbstract::KEY_COMPONENTS)[Db::class];
            $dependencies = $config['dependencies'];
            $db->configure($dependencies);
        }
        return $db;
    }
}