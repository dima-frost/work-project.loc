<?php


namespace Core\Db;


use Core\Application;

abstract class Model implements ActiveRecordInterface
{
    protected $attributes = [];

    public function __construct()
    {
    }

    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }

        return null;
    }

    public function __isset($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    abstract static protected function tableName();

    public static function find($condition = [])
    {
        /** @var QueryBuilder $builder */
        $builder = new QueryBuilder();
        $builder->setModel(static::class);
        $builder->table(static::tableName());
        if ($condition) {
            $builder->where($condition);
        }
        return $builder;
    }

    public static function read($id)
    {
        /** @var QueryBuilder $builder */
        $builder = new QueryBuilder();
        $builder->setModel(static::class);
        $model = $builder->table(static::tableName())->where(['id' => $id])->one();
        return $model;
    }

    public function save()
    {
        if ($this->exists()) {
            return $this->update();
        } else {
            return $this->create();
        }
    }

    public function delete()
    {
        return Application::getInstance()->get(Db::class)->query("DELETE FROM " . $this->tableName()
                . " WHERE id = " . $this->id);
    }

    public function create()
    {
        $this->created_at();
        $this->updated_at();
        foreach ($this->attributes as $key => $value) {
            $attributes[$key] = "'" . $value . "'";
        }
        $query = "INSERT INTO " . static::tableName() . " (" . implode(',', array_keys($attributes)) . ")"
                . " VALUES (" . implode(',', $attributes) . ")";
        if ($id = Application::getInstance()->get(Db::class)->insert($query)) {
            $this->id = $id;
        }

        return $this;
    }

    public function update()
    {
        $this->updated_at();
        foreach ($this->attributes as $key => $value) {
            $attributes[$key] = $key . "='" . $value . "'";
        }
        $query = "UPDATE " . static::tableName() . " SET " . implode(',', $attributes) . " WHERE id = " . $this->id;
        Application::getInstance()->get(Db::class)->query($query);

        return $this;
    }

    /**
     * @return bool
     * @todo Сделать проверку реального существования записи в базе данных
     */
    protected function exists()
    {
        return isset($this->id);
    }

    protected function fill($attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public static function createModel($attributes)
    {
        $model = new static();
        $model->fill($attributes);
        return $model;
    }

    public static function createModels($rows)
    {
        $models = [];
        foreach ($rows as $row) {
            $models[] = static::createModel($row);
        }
        return $models;
    }
    public function created_at(){
        $this->attributes['created_at']=date('Y-m-d H:i:s');
    }
    public function updated_at(){
        $this->attributes['updated_at']=date('Y-m-d H:i:s');
    }
}
