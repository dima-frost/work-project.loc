<?php


namespace Core\Db;


interface ConnectionInterface
{
    public static function getInstance();
    public function configure();
    public function dbConnect();


}