<?php


namespace Core\Db;


interface ConfigurableInterface
{
    public function configure(array $config);

}