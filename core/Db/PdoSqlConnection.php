<?php


namespace Core\Db;


use PDO;

class PdoSqlConnection implements ConnectionInterface
{


    private static $instance;
    private $driver;
    private $host;
    private $database;
    private $user;
    private $pass;

    public static function getInstance()
    {
        if(is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;

    }


    public function configure()
    {
        $configDb = include $_SERVER['DOCUMENT_ROOT'] . '/../config/db.php';
        $this->driver = $configDb['driver'];
        $this->host = $configDb['host'];
        $this->database = $configDb['database'];
        $this->user = $configDb['user'];
        $this->pass = $configDb['pass'];
    }

    public function dbConnect()
    {
        $this->configure();
        $dsn = "$this->driver:host=$this->host;dbname=$this->database;charset = utf8";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $pass = $this->pass;

     $db =  new PDO($dsn, $this->user, $pass, $opt);
       return $db;


    }
}