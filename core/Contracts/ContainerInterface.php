<?php

namespace Core\Contracts;


interface ContainerInterface
{
    public function has($name);

    public function get($name);
}