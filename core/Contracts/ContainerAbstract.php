<?php


namespace Core\Contracts;


abstract class ContainerAbstract implements ContainerInterface
{
    /**
     * Эта константа используется для конфигурации компонентов
     */
    public const KEY_COMPONENTS = 'components';

    protected $config = [];

    /**
     * @var array Массив привязки компонентов к контейнеру
     */
    protected $components = [];

    /**
     * @var array Массив экземпляров компонентов
     */
    protected $instances = [];

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __set($name, $value)
    {
        $this->components[$name] = $value;
    }

    /**
     * @param $config
     */
    public function configure($config)
    {
        $this->config = $config;

        $this->bootstrap();
    }

    public function bootstrap()
    {
        if (isset($this->config[self::KEY_COMPONENTS]) && is_array($this->config[self::KEY_COMPONENTS])) {
            foreach ($this->config[self::KEY_COMPONENTS] as $name => $component) {
                //$this->createObject($name, $component);
                $this->components[$name] = $component;
            }
        }
    }

    protected function createObject($name)
    {
        if (array_key_exists($name, $this->components)) {
            $component = $this->components[$name];
            if (isset($component['factory']) && class_exists($component['factory'])) {
                $factory = new $component['factory']($this);
                $instance = $factory->createInstance();

                return $instance;
            } elseif(is_array($component) && array_key_exists('class', $component)) {
                return $this->resolveService($component);
            }
        }

        throw new \Exception('Can not instantiate object ' . $name);
    }

    public function has($name)
    {
        return array_key_exists($name, $this->components);
    }

    /**
     * Метод получает с контейнера компонент и возвращает его
     *
     * Данный метод проверяет есть ли зарегистрированный компонент.
     * Если такой компонент существует, будет возращет его экземпляр...
     *
     * @param $name
     * @return ComponentAbstract|null
     */
    public function get($name)
    {
        if (!array_key_exists($name, $this->instances)) {
            $instance = $this->createObject($name);
            $this->instances[$name] = $instance;
        }

        return $this->instances[$name];
    }

    public function config($name = null)
    {
        if (is_null($name)) {
            return $this->config;
        }

        if (array_key_exists($name, $this->config)) {
            return $this->config[$name];
        }

        return null;
    }

    protected function resolveService($config)
    {
        $reflectionClass = new \ReflectionClass($config['class']);
        $constructor = $reflectionClass->getConstructor();
        if ($constructor === null) {
            return $reflectionClass->newInstance();
        }
        $dependencies = $this->resolveDependencies($constructor, $config);
        return $reflectionClass->newInstanceArgs($dependencies);
    }

    public function resolveDependencies(\ReflectionFunctionAbstract $method, $params=[])
    {
        $dependencies = [];
        foreach ($method->getParameters() as $parameter) {
            $name = $parameter->getName();
            if (array_key_exists($name, $params)) {
                $dependencies[$name] = $params[$name];
            } elseif (isset($params['dependencies']) && array_key_exists($name, $params['dependencies'])) {
                $dependencies[$name] = $this->get($params['dependencies'][$name]);
            } elseif ($parameter->getType() && !$parameter->getType()->isBuiltin()) {
                $dependencies[$name] = $this->get($parameter->getType()->getName());
            }
        }
        return $dependencies;
    }
}