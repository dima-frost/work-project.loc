<?php


namespace Core\Contracts;


interface BootstrapInterface
{
    function bootstrap();
}