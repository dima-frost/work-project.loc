<?php


namespace Core\Contracts;


interface ConfigurableInterface
{
    public function configure(array $config);
}