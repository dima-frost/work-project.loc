<?php


namespace Core\Observer;


interface ObserverInterface

{
    public function listen($event, $data = []);
}