<?php


namespace Core\Observer;


class ObserverManager implements SubjectInterface
{

    /**
     * @var ObserverInterface[]
     */
    protected array $observers = [];

    public function attach(ObserverInterface $observer, $event = '*')
    {
        $events = (array) $event;
        foreach ($events as $value){
            $this->observers[$value][] = $observer;
        }


    }

    public function detach(ObserverInterface $observer, $event = '*')
    {
       foreach ($this->observers[$event] as $key => $items){
           if ($observer === $items){
               unset($this->observers[$key]);
           }
       }
    }

    public function notify($event, $data = [])
    {   $info = $data;

        $all = $this->observers['*'];
  //      $observers = array_merge($this->observers[$event], $all);
        foreach ($all as $observer) {
            if (!empty($observer)) {
                $observer->listen($event,$info);
            }
        }
    }
}