<?php


namespace Core\Observer;


interface SubjectInterface
{
    public function attach(ObserverInterface $observer, $events);
    public function detach(ObserverInterface $observer, $events);
    public function notify($event, $data = []);

}