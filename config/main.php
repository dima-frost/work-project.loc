<?php

use App\Components\Api\CustomApiGuzzle;
use Core\Db\Db;
use Core\Db\DbFactory;
use Core\Logger\FileWriterLogs;
use Core\Logger\LoggerFactory;
use Core\Logger\Formatter;
use Core\Routing\RouterFactory;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

return [
    'components' => [
        RouterFactory::KEY_INSTANCE => [
            'factory' => RouterFactory::class,
            'dependencies' => [
                'file' => $_SERVER['DOCUMENT_ROOT'] . '/../app/Http/routes.php',
            ]
        ],
        'logger' => [
            'factory' => LoggerFactory::class,
            'dependencies' => [
                'writer' => FileWriterLogs::class,
                'formatter' => Formatter::class,
            ]
        ],
        CustomApiGuzzle::class => [
            'class' => CustomApiGuzzle::class,
            'dependencies'=>[
                'client' => ClientInterface::class
            ],
        ],
        ClientInterface::class=>[
            'class' => Client::class,
            'config' => [
                'base_uri'=>'https://gorest.co.in/',
            ],
        ],
        Db::class => [
            'factory' => DbFactory::class,
            'dependencies' => [
                'host' => 'localhost',
                'port' => 3306,
                'user' => 'root',
                'pass' => 'root123!@#',
                'dbname' => 'mvc',
            ]
        ]
    ],

];